import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import Navbar from '../../Navbar/Navbar'
import Footer from '../Footer/Footer'
import './nouser.css'

import Background from '../../../imgs/background.svg'

const NoUser = props => <div className="background_results" style={{backgroundImage: `url('${Background}')`}}>
        <Navbar />
    <Container className="no_user_container">
        <Row className="text-center">
            <Col xs="12" className="margin_bottom_100">
                <h1>This user is not participating in the Token Challenge</h1>
            </Col>
            <Col xs="12">
                <Footer/>
            </Col>
            <div className="extra" />
        </Row>
    </Container>
</div>

export default NoUser