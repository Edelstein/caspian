import React, {Component} from 'react';
import moment from 'moment'
export default class Seconds extends Component {
    constructor(props) {
        super(props);
        this.state = {
            minutes:0,
        };
    }
    
    componentWillMount() {
        this.getTimeUntil(this.props.deadline);
    }

    componentDidMount() {
        setInterval(()=>this.getTimeUntil(this.props.deadline),1000);
    }
    
    
    getTimeUntil(deadline){
        const time = moment(deadline).diff(moment());
        const seconds = Math.floor((time/1000)%60);
        this.setState({seconds});
    }
    render() {
        return (
             <div>{this.state.seconds > 0 ? this.state.seconds <= 9 ? `0${this.state.seconds}` : this.state.seconds.toString() : '00'}</div>
        );
    }
}