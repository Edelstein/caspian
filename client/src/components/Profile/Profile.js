import React , { Component } from 'react'
import { Container, Row, Col } from 'reactstrap'
import { Redirect } from 'react-router-dom'
import Navbar from '../Navbar/Navbar'
import Loader from '../parts/Loader/Loader'
import moment from 'moment'
import TransitionGroup from 'react-transition-group/TransitionGroup'
import _ from 'lodash'
import axios from 'axios'
import achievs from '../../text/achievements'
import Footer from '../parts/Footer/Footer'

import Pattern from '../../imgs/pattern.png'
import Mark from '../../imgs/tooltip.svg'
import Box from '../../imgs/tooltipbox.svg'
import MobileBox from '../../imgs/tooltipboxmobile.svg'
import Token from '../../imgs/reward.svg'
import Default from '../../imgs/profile/noimg.svg'
import Task from '../../imgs/profile/tasks.svg'
import Coin from '../../imgs/profile/tokens.svg'
import Background from '../../imgs/background.svg'
import BackgroundMobile from '../../imgs/backgroundmobile.svg'
import './profile.css'
import { API_ADDRESS } from '../../config/keys'

class Profile extends Component {
    constructor() {
        super()
            this.state = {
            user: {
                first_name: '',
                username: '',
            },
            message_count: 0,
            invite_count: 0,
            loading_one: true,
            loading_two: true,
            loading_three: true,
            loading_four: true,
            loading_five: true,
            loading_six: true,
            achievements: []
        }
    }

    getDetails(id) {
        let sum = 0
        axios.get(`${API_ADDRESS}/api/users/messageCount?id=${id}&group_id=-1001167096375`).then(res => {
            if(res.data) {
                if(res.data.ok) {
                    this.setState({ message_count: res.data.result, loading_one: false })
                }
            }
        })

        axios.get(`${API_ADDRESS}/api/users/userphotoid?id=${id}&group_id=-1001167096375`).then(res => {
            if(res.data) {
                if(res.data.ok) {
                    this.setState({ image: res.data.result.file_id, loading_two: false })
                } else {
                    this.setState({ image: false, loading_two: false })
                }
            }
        })
        axios.get(`${API_ADDRESS}/api/users/inviteCount?id=${id}&group_id=-1001167096375`).then(res => {
            if(res.data) {
                if(res.data.ok) {
                    this.setState({ invite_count: res.data.result, loading_three: false })
                }
            }
        })
        axios.get(`${API_ADDRESS}/achievements/user/getbyid?id=${id}&group_id=-1001167096375`).then(res => {
            if(res.data.result === 'ok') {
                this.setState({ user: res.data.user, loading_four: false })
            } else if (res.data.result === 'bad') {
                this.setState({ no_user: true })
            }
        })
        axios.get(`${API_ADDRESS}/achievements/user/all?user_id=${id}&group_id=-1001167096375`).then(res => {
            if(res.data.result === 'ok') {
                this.setState({ loading_five: false })
                const date = res.data.user_achievements.filter(n => n.achievement === 0)
                if(res.data.user_achievements.length === 0) {
                    this.setState({no_user: true})
                } else if(!_.includes(res.data.user_achievements.map(n => n.achievement), 0)) {
                    this.setState({no_user: true})
                }
                if(date.length === 1) {
                    this.setState({ join_date: date[0].created_at})
                }
                console.log(res.data.user_achievements.map(n => n.achievement).filter(n => n !== 11 && n !== 12 && n !== 5), 'hello')
                res.data.user_achievements.map(n => {
                    switch(n.achievement) {
                        case 0:
                            return sum += 10
                        case 1:
                            return sum += 50
                        case 2:
                            return sum += 80
                        case 4:
                            return sum += 30
                        case 3:
                            return sum += 40
                        case 6:
                            return sum += 30
                        case 22:
                            return sum += 30
                        case 23:
                            return sum += 30
                        case 9:
                            return sum += 60
                        case 10:
                            return sum += 100
                        default:
                            return 0
                    }
                })
                this.setState({tasks: res.data.user_achievements.map(n => n.achievement).filter(n => n !== 11 && n !== 12 && n !== 5).length, tokens: sum, achievements: res.data.user_achievements.map(n=> n.achievement) })

            } else if (res.data.result === 'bad') {
                this.setState({ no_achievs: true, no_user: true, loading_one: false })
            }
        })
    }

    componentWillReceiveProps(nextProps) {
        if(this.props.match.params.id !== nextProps.match.params.id) {
            this.getDetails(nextProps.match.params.id)
        }
    }
    componentDidMount() {
        window.scrollTo(0, 0)
        window.addEventListener("resize", this.handleResize.bind(this))
        this.getDetails(this.props.match.params.id)
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize.bind(this))
    }
    render() {
        const is_phone = window.matchMedia("(max-width: 768px)").matches
        const { tokens, achievements, join_date, tasks, invite_count, message_count, image, no_user, loading_one, loading_two, loading_three, loading_four, loading_five, loading_six, facebook_tooltip  } = this.state
        return( 
            <div className="background" style={{backgroundImage: `url('${is_phone ? BackgroundMobile : Background}')`}}>
                <Navbar />
                {/* {loading_one || loading_two || loading_three || loading_four || loading_five  ? <div className="profile_loading_container margin_top_200"><Loader /></div> : <div className="profile_wrapper"> */}
                {loading_one || loading_two || loading_three || loading_four || loading_five  ? <div className="profile_loading_container margin_top_200"><Loader /></div> : no_user ? <Redirect to='/nouser' /> : <div className="profile_wrapper"> 
                                   <Container>
                    <Row className="text-center">
                        <Col xs="12">
                            <h1 className="profile_margin hello_username">Hello {this.state.user ? this.state.user.username ? this.state.user.username : this.state.user.first_name : null}</h1>
                        </Col>
                        <Col xs="12">
                            <p className="intro_text" >Here you can see your personal stats</p>
                        </Col>
                    </Row>
                    <Row className="margin_top_profile_page justify-content-center">
                        <Col lg="5" md="12" xs="12" className="user_details">
                            {image ? <img style={{border: '4px solid #00dac3'}} src={`http://images.obelisk.ai:3000/imgs/${image}.jpg`} className="profile_img" /> : <img src={Default} className="profile_img" />}
                        </Col>
                        <Col lg="7" md="12" xs="12" className="user_details ">
                            <h2 className="username margin_bottom_60">{this.state.user ? this.state.user.username ? `@${this.state.user.username}` : this.state.user.first_name : null}</h2>
                            <p className="margin_top_date">Member since: <strong>{moment(join_date).format('DD/MM/YY')}</strong></p>
                            <Row className="detail_row">
                                <Col lg="3" md="6" xs="6" className="align_detail"><p><span className="text_lg">{tokens >= 225 ? 225 : tokens}</span></p><img src={Coin} alt="coin" className="filler"/></Col>
                                <Col lg="3" md="6" xs="6" className="align_detail tasks_details"><p><span className="text_lg">{tasks}</span></p><img src={Task} alt="coin" className="trophy"/></Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
                <div style={{backgroundImage: `url('${Pattern}')`}} className="achievements_background_container">
                <Row className="text-center  justify-content-center drag_bottom">
                        <Col xs="12" className=" intro_achievements_margins">
                            <h2><strong className="secondary_title ">Achievements</strong></h2>
                        </Col>
                    </Row>
                    <TransitionGroup>
                <div className="achievements_container">
                {/* join telegram group */}
                    <div className="achievement margin_top_100">
                        <div className="center_col margin_achievement_image">
                            <img src={achievs[0].img} className={`achievement_img_container`} />
        {is_phone ? null : <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[0].reward} CSP</p> }
                        </div>

                        <div className="achievement_text_container" >
                            <div className="achievement_title_background">
                            <a href={achievs[0].link} className="achievement_anchor" target="_blank" rel="noopener"><h2 className="achievement_title  ">{achievs[0].title}</h2></a>
                                </div>
                                <p className="achievement_text opensans">{achievs[0].text}</p>
                            
                        </div>
        {is_phone ? <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[0].reward} CSP</p>  : null}

                        <div className="achievement_progressbar">
                            <div style={{width: _.includes(achievements, 0) ? '100%' : '0%', borderRadius: _.includes(achievements, 0) ? '0px 0px 5px 5px' : ''}} className="achievement_progress">
                                {_.includes(achievements, 0) ? null : <span className="amount_currently_placeholder">0</span>}
                            </div>
                            <span className="amount_to_complete">1</span>
                        </div>
                    </div>
                {/* join telegram group */}

                {/* invite 3 friends */}
                <div className="achievement margin_top_120">
                        <div className="center_col margin_achievement_image">
                            <img src={achievs[1].img} className={`achievement_img_container`} />
                            {is_phone ? null : <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[1].reward} CSP</p>}
                        </div>

                        <div className="achievement_text_container" >
                            <div className="achievement_title_background">
                                <h2 className="achievement_title  ">{achievs[1].title}</h2>
                                </div>
                                <p className="achievement_text opensans">{achievs[1].text}</p>
                            
                        </div>
                        {is_phone ? <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[1].reward} CSP</p> : null}

                        <div className="achievement_progressbar">
                            <div style={{width: invite_count >= 3 ? '100%' : `${invite_count*33.3}%`, borderRadius: invite_count >= 3 ? '0px 0px 5px 5px' : ''}} className="achievement_progress">
                                {invite_count >= 3 ? null : invite_count >= 1 ? <span className="amount_currently">{invite_count}</span> : invite_count === 0 ? <span className="amount_currently_placeholder">{0}</span> : null}
                            </div>
                            <span className="amount_to_complete">3</span>
                        </div>
                    </div>
                {/* invite 3 friends */}

                {/* invite 7 friends */}
                <div className="achievement margin_top_120">
                        <div className="center_col margin_achievement_image">
                            <img src={achievs[2].img} className={`achievement_img_container`} />
                            {is_phone ? null : <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[2].reward} CSP</p>}
                        </div>

                        <div className="achievement_text_container" >
                            <div className="achievement_title_background">
                                <h2 className="achievement_title  ">{achievs[2].title}</h2>
                                </div>
                                <p className="achievement_text opensans">{achievs[2].text}</p>
                            
                        </div>
                        {is_phone ? <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[2].reward} CSP</p> : null}

                        <div className="achievement_progressbar">
                            <div style={{width: invite_count >= 7 ? '100%' : `${invite_count*14.28}%`, borderRadius: invite_count >= 7 ? '0px 0px 5px 5px' : ''}} className="achievement_progress">
                                {invite_count >= 1 ? <span className="amount_currently">{invite_count}</span> : invite_count === 0 ? <span className="amount_currently_placeholder">{0}</span> : null}
                            </div>
                            <span className="amount_to_complete">7</span>
                        </div>
                    </div>
                {/* invite 7 friends */}

                {/* advisor */}
                <div className="achievement margin_top_120">
                        <div className="center_col margin_achievement_image">
                            <img src={achievs[3].img} className={`achievement_img_container`} />
        {is_phone ? null : <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[3].reward} CSP</p> }
                        </div>

                        <div className="achievement_text_container" >
                            <div className="achievement_title_background">
                                <h2 className="achievement_title  ">{achievs[3].title}</h2>
                                </div>
                                <p className="achievement_text opensans">{achievs[3].text}</p>
                            
                        </div>
        {is_phone ? <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[3].reward} CSP</p> : null}

                        <div className="achievement_progressbar">
                            <div style={{width: _.includes(achievements, 4) ? '100%' : '0%', borderRadius: _.includes(achievements, 4) ? '0px 0px 5px 5px' : ''}} className="achievement_progress">
                                {_.includes(achievements, 4) ? null : <span className="amount_currently_placeholder">0</span>}
                            </div>
                            <span className="amount_to_complete">1</span>
                        </div>
                    </div>
                {/* advisor */}

                {/* conversation starter */}
                <div className="achievement margin_top_120">
                        <div className="center_col margin_achievement_image">
                            <img src={achievs[4].img} className={`achievement_img_container`} />
                            {is_phone ? null : <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[4].reward} CSP</p>}
                        </div>

                        <div className="achievement_text_container" >
                            <div className="achievement_title_background">
                                <h2 className="achievement_title  ">{achievs[4].title}</h2>
                                </div>
                                <p className="achievement_text opensans">{achievs[4].text}</p>
                            
                        </div>
                        {is_phone ? <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[4].reward} CSP</p> : null}
                        <div className="achievement_progressbar">
                            <div style={{width: message_count >= 15 ? '100%' : `${message_count*6.66}%`, borderRadius: message_count >= 15 ? '0px 0px 5px 5px' : ''}} className="achievement_progress">
                                {message_count >= 15 ? null : message_count >= 1 ? <span className="amount_currently">{message_count}</span> : message_count === 0 ? <span className="amount_currently_placeholder">{0}</span> : null}
                            </div>
                            <span className="amount_to_complete">15</span>
                        </div>
                    </div>
                {/* conversation starter */}

                {/* twitter */}
                <div className="achievement margin_top_120">
                        <div className="center_col margin_achievement_image">
                            <img src={achievs[5].img} className={`achievement_img_container`} />
                           {is_phone ? null : <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[5].reward} CSP</p>}
                        </div>

                        <div className="achievement_text_container" >
                            <div className="achievement_title_background">
                            <a href={achievs[5].link} className="achievement_anchor" target="_blank" rel="noopener"> <h2 className="achievement_title  ">{achievs[5].title}</h2></a>
                                </div>
                                <p className="achievement_text opensans">{achievs[5].text}</p>
                            
                        </div>
                        {is_phone ? <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[5].reward} CSP</p> : null}
                        <div className="achievement_progressbar">
                            <div style={{width: _.includes(achievements, 6) ? '100%' : '0%', borderRadius: _.includes(achievements, 6) ? '0px 0px 5px 5px' : ''}} className="achievement_progress">
                                {_.includes(achievements, 6) ? null : <span className="amount_currently_placeholder">0</span>}
                            </div>
                            <span className="amount_to_complete">1</span>
                        </div>
                    </div>
                {/* twitter */}

                {/* medium */}
                <div className="achievement margin_top_120">
                        <div className="center_col margin_achievement_image">
                            <img src={achievs[6].img} className={`achievement_img_container`} />
                            {is_phone ? null : <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[6].reward} CSP</p>}
                        </div>

                        <div className="achievement_text_container" >
                        <div>
                            <img onClick={() => this.setState({facebook_tooltip: !this.state.facebook_tooltip})} src={Mark} className="facebook_tooltip" />
                            {facebook_tooltip ? <div  className="facebook_tooltip_box" style={{backgroundImage: `url('${Box}')`}} >
                            <span className="facebook_tooltip_text">Go to the <a href="https://t.me/crowdnetworkbot?start=Caspian" target="_blank" rel="noopener">Telegram Bot</a> and type /medium</span>
                            </div> : null}
                            </div>
                            <div className="achievement_title_background">
                            <a href={achievs[6].link} className="achievement_anchor" target="_blank" rel="noopener"><h2 className="achievement_title  ">{achievs[6].title}</h2></a>
                                </div>
                                <p className="achievement_text opensans">{achievs[6].text}</p>
                            
                        </div>
                        {is_phone ? <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[6].reward} CSP</p> : null}
                        <div className="achievement_progressbar">
                            <div style={{width: _.includes(achievements, 22) ? '100%' : '0%', borderRadius: _.includes(achievements, 22) ? '0px 0px 5px 5px' : ''}} className="achievement_progress">
                                {_.includes(achievements, 22) ? null : <span className="amount_currently_placeholder">0</span>}
                            </div>
                            <span className="amount_to_complete">1</span>
                        </div>
                    </div>
                {/* medium */}

                {/* participate in quiz bot */}
                <div className="achievement margin_top_120">
                        <div className="center_col margin_achievement_image">
                            <img src={achievs[7].img} className={`achievement_img_container`} />
                            {is_phone ? null : <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[7].reward} CSP</p>}
                        </div>

                        <div className="achievement_text_container" >
                            <div className="achievement_title_background">
                                <h2 className="achievement_title  ">{achievs[7].title}</h2>
                                </div>
                                <p className="achievement_text opensans">{achievs[7].text}</p>
                            
                        </div>
                        {is_phone ? <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[7].reward} CSP</p> : null}

                        <div className="achievement_progressbar">
                            <div style={{width: _.includes(achievements, 23) ? '100%' : '0%', borderRadius: _.includes(achievements, 23) ? '0px 0px 5px 5px' : ''}} className="achievement_progress">
                                {_.includes(achievements, 23) ? null : <span className="amount_currently_placeholder">0</span>}
                            </div>
                            <span className="amount_to_complete">1</span>
                        </div>
                    </div>
                {/* participate in quiz bot */}

                {/* win quiz bot */}
                <div className="achievement margin_top_120">
                        <div className="center_col margin_achievement_image">
                            <img src={achievs[8].img} className={`achievement_img_container`} />
                            {is_phone ? null : <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[8].reward} CSP</p>}
                        </div>

                        <div className="achievement_text_container" >
                            <div className="achievement_title_background">
                                <h2 className="achievement_title  ">{achievs[8].title}</h2>
                                </div>
                                <p className="achievement_text opensans">{achievs[8].text}</p>
                            
                        </div>
                        {is_phone ? <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[8].reward} CSP</p> : null}

                        <div className="achievement_progressbar">
                            <div style={{width: _.includes(achievements, 9) ? '100%' : '0%', borderRadius: _.includes(achievements, 9) ? '0px 0px 5px 5px' : ''}} className="achievement_progress">
                                {_.includes(achievements, 9) ? null : <span className="amount_currently_placeholder">0</span>}
                            </div>
                            <span className="amount_to_complete">1</span>
                        </div>
                    </div>
                {/* win quiz bot */}

                {/* win quiz bot */}
                <div className="achievement margin_top_120">
                        <div className="center_col margin_achievement_image">
                            <img src={achievs[9].img} className={`achievement_img_container`} />
                            {is_phone ? null : <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[9].reward} CSP</p>}
                        </div>

                        <div className="achievement_text_container" >
                            <div className="achievement_title_background">
                                <h2 className="achievement_title  ">{achievs[9].title}</h2>
                                </div>
                                <p className="achievement_text opensans">{achievs[9].text}</p>
                            
                        </div>
                        {is_phone ?  <p className="achievement_reward achievement_reward_profile_mobile"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {achievs[9].reward} CSP</p> : null}
                        <div className="achievement_progressbar">
                            <div style={{width: _.includes(achievements, 10) ? '100%' : '0%', borderRadius: _.includes(achievements, 10) ? '0px 0px 5px 5px' : ''}} className="achievement_progress">
                                {_.includes(achievements, 10) ? null : <span className="amount_currently_placeholder">0</span>}
                            </div>
                            <span className="amount_to_complete">1</span>
                        </div>
                    </div>
                {/* win quiz bot */}

                </div>
                </TransitionGroup>
                <Footer />
                </div>
                </div>
            }
            </div>
        )
    }
    handleResize() {
        this.setState({width: window.innerWidth, height: window.innerHeight})
    }
}

export default Profile