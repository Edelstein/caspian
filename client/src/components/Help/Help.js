import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import Navbar from '../Navbar/Navbar'
import Footer from '../parts/Footer/Footer'
import Pattern from '../../imgs/pattern.png'
import './help.css'

const Help = props => <div>
    <Navbar />
    {window.scrollTo(0, 0)}
    <Container>
        <Row className="text-center justify-content-center">
            <Col xs="12">
                <h1 className="helpbot_title">Help bot</h1>
            </Col>
            <Col xs="12" className="">
                <p className="intro_text" ><strong><a className="to_helpbot" href="https://t.me/crowdnetworkbot?start=Caspian" target="_blank" rel="noopener">Click Here</a> and type /help in the chat.</strong></p>
             </Col>
             <Footer />
        </Row>
    </Container>
    <div style={{backgroundImage: `url('${Pattern}')`}} className="achievements_background_container helpbot_footer">
            <Footer />
        </div>
</div>

export default Help