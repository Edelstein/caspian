import React from 'react'
import { Row, Col } from 'reactstrap'
import Box from '../../../imgs/percent_box.svg'

const ProgressBar = props =><Row className="justify-content-center">
<Col xs="10" md="12">
<div className="progressbar">
<div style={{width: `${props.percentage < 1 ? props.is_phone ? '3' :  '1' : props.percentage}%`}} className="progress_location">
    <div style={{backgroundImage: `url('${Box}')`}} className="percent_box" >
    <p className="progressbar_percentage">{parseInt(props.percentage)}%</p>
    </div>
</div>
</div>
</Col>
</Row>

export default ProgressBar