import { GET_INPUT } from '../actions/types'

export default function(state = '', action) {
  switch (action.type) {
    case GET_INPUT:
      return action.payload || false;
    default:
      return state
  }
}