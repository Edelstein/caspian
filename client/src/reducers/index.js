import { combineReducers } from 'redux'
import users_reducer from './usersReducer'
import search_reducer from './searchReducer'

export default combineReducers({
  users: users_reducer,
  search: search_reducer
})