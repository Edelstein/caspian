import join from '../imgs/achievements/join.svg'
import quizbotwin from '../imgs/achievements/quizbotwin.svg'
import medium from '../imgs/achievements/medium.svg'
import advisor from '../imgs/achievements/advisor.svg'
import invitetotg from '../imgs/achievements/invitetotg.svg'
import jackpot from '../imgs/achievements/jackpot.svg'
import conversationstarter from '../imgs/achievements/conversationstarter.svg'
import quizbot from '../imgs/achievements/quizbot.svg'
import React from 'react'
import invitemoretotg from '../imgs/achievements/invitemoretotg.svg'
import twitter from '../imgs/achievements/twitter.svg'



const achievements = [
    {
        title: 'Joining The Community',
        link: 'https://t.me/Caspian_Tech',
        text: 'Join the Telegram group! Engage with the Caspian community until the ICO closes.',
        img: join,
        reward: 10
    }, 
    {
        title: 'Inviting 3 Friends',
        link: '',
        text: 'Invite 3 friends to Caspian’s Telegram community. Once you join the group, simply invite them through Telegram. No link required.',
        img: invitetotg ,
        reward: 50
    }, 
    {
        title: 'Inviting Additional 4 Members',
        text: 'Invite an additional 4 members (7 in total) to Caspian’s Telegram group. Tip: the more you invite, the more likely it is that 7 will stay.',
        link: '',
        img: invitemoretotg,
        reward: 80
    }, 
    {
        title: 'Answer Member’s Question',
        link: '',
        text: 'Help a friend out! Answer another community member’s question in Telegram.',
        img: advisor,
        reward: 30
    }, 
    {
        title: 'Conversation Starter',
        link: '',
        text: 'Help us get quality engagement going on Telegram by sending 15 messages back and forth with other community members.',
        img: conversationstarter,
        reward: 40
    }, 
    {
        title: `Follow Caspian's Twitter`,
        link: 'https://twitter.com/Caspian_Tech',
        text: 'Join us on Twitter for the latest Caspian news. Remember to stay as a follower until the end of the ICO.',
        img: twitter,
        reward: 30
    }, 
    {
        title: 'Medium Maximum Clap',
        link: 'https://medium.com/@Caspian_Tech',
        text: 'Max clap (50 times) Caspian’s 3 most recent blog posts.',
        tooltip: 'and type /medium',
        img: medium,
        reward: 30
    }, 
    {
        title: `Participate in Caspian's Trivia`,
        link: '',
        text: 'Take part and play in the ongoing trivia game in our Telegram group. You have 10 minutes for each question. (One reward per community member).',
        img: quizbot,
        reward: 30
    }, 
    {
        title: `Win in Caspian's Trivia`,
        link: '',
        text: 'Be the first to answer the correct answer in the trivia game!',
        img: quizbotwin,
        reward: 60
    }, 
    {
        title: 'Complete All Achievements',
        link: '',
        text: 'Complete all other achievements and you’ll automatically unlock this badge.',
        img: jackpot,
        reward: 100
    }
]

export default achievements