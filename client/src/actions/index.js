import { GET_INPUT, GET_USERS } from './types'

export const sendToResults = users => async dispatch => {
  dispatch({ type: GET_USERS, payload: users })
}

export const input = input => async dispatch => {
    dispatch({ type: GET_INPUT, payload: input })
  }